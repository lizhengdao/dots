#!/usr/bin/env bash

export BASE=$(realpath ${BASE:-$(pwd)})

if [ ! -d vett ]; then
  git clone --depth 1 https://github.com/subins2000/vett.git vett
else
  cd vett && git pull && cd -
fi

cd vett

if [ -d vett ]; then 
  rm -rf vett dist
fi

yarn install
yarn build
mv dist vett # rename

rm vett.zip
zip vett.zip -r vett/*

mv vett.zip ../app/src/main/assets