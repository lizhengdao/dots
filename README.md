# Dots, The Dots & Boxes Game You Played In School

## Setup

* [Set submodules](https://stackoverflow.com/a/10168693/1372424)

```
git submodule update --init --recursive
```

* Build vett (the game) source :

```
./build-vett.sh
```

* Open in Android Studio and build app

## Submodules

These are the submodules used :

```
git submodule add -b app-not-sslapp --depth 1 https://github.com/subins2000/openwebtorrent-tracker.git app/src/main/cpp/openwebtorrent-tracker
```